package Prog.CSDbcas.bank;

public class BankAccount {
	public static final String bankName = "PEOPLE'S BANK";
	private String branch;
	private double accBalance = 0;
	private String accType;
	private String accNumber;
	private String accHolderName;
	
	public void accountOpen(String branch,String accName,String accNumber,String accHolderName,int amount) {
		
		this.branch = branch;
		this.accType = accName;
		this.accNumber = accNumber;
		this.accHolderName = accHolderName;
		this.accBalance = amount;
		
	}
	
	public void withdrawal (double amount) {
		accBalance = accBalance - amount;
		
	}
	public void deposit(double amount) {
		accBalance = accBalance + amount;
		
	}
	public double AccBalance() {
		return accBalance;
		
	}
	public String AccountHolderName() {
		return accHolderName;
		
	}
	public void AccountDetatils() {
		
		System.out.println("BANK NAME : " + bankName + "-"+branch);
		System.out.println("ACCOUNT NUMBER : " + accNumber);
		System.out.println("ACCOUNT TYPE : " + accType);
		System.out.println("ACCOUNT HOLDER NAME : " + accHolderName);
		System.out.println("ACCOUNT BALANCE : " + accBalance);
		System.out.println ("**********************************");
				
	}
}
